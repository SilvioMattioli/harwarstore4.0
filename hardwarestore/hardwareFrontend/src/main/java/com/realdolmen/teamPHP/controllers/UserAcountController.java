package com.realdolmen.teamPHP.controllers;

import com.realdolmen.teamPHP.domain.ShopBasket;
import com.realdolmen.teamPHP.dtos.UserDTO;
import com.realdolmen.teamPHP.facade.UserFacade;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class UserAcountController implements Serializable {

    @Inject
    private UserFacade userFacade;

    private Long id;
    private String name;
    private ShopBasket shopbasket;

    @PostConstruct
    public void init() {

    }

    public UserFacade getUserFacade() {
        return userFacade;
    }

    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ShopBasket getShopbasket() {
        return shopbasket;
    }

    public void setShopbasket(ShopBasket shopbasket) {
        this.shopbasket = shopbasket;
    }

    public void registeruser(String userl){
        UserDTO user = new UserDTO(null,userl);
        userFacade.saveUser(user);
    }

}
