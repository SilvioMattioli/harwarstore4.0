package com.realdolmen.teamPHP.controllers;

import com.realdolmen.teamPHP.dtos.ProductDTO;
import com.realdolmen.teamPHP.facade.ProductFacade;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.swing.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;



//@ManagedBean(name = "productController")
@Named
@SessionScoped
public class ProductController implements Serializable {
    private List<ProductDTO> productDTOS;
    private ProductDTO productDTO;
    HashMap<String,String>userPass=new HashMap<String, String>();
    JFrame frame = new JFrame();




    private  String type;
    private String id;

    @Inject
    private ProductFacade productFacade;

    public ProductController() {
    }

    
    @PostConstruct
    public void init() {
        System.out.println("In init method");
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        type = params.get("type");
        System.out.println(type);
        userPass.put("willy","wanka");
        userPass.put("kill","bill");
        userPass.put("steven","verslapen");



    }
    public void setProductDTOS(List<ProductDTO> productDTOS) {
        this.productDTOS = productDTOS;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public List<ProductDTO> getAllProducts(String string) {
        productDTOS = productFacade.findAll(string);
        return productDTOS;
    }
    public ProductDTO getProduct (String productType,String id) {
        productDTO = productFacade.findProduct(productType,Long.parseLong(id));
        return productDTO;
    }

    public String login(String L_user, String L_Password){

        if (userPass.containsKey(L_user)){
            //System.out.println("user excits");
             //String key =userPass.containsKey(L_user);


            //if (userPass.get(L_user) == L_Password){
            if (L_Password.equals(userPass.get(L_user))){

                System.out.println("passwoord is correct");
                return "home-hardware.xhtml";
            }else {
                JOptionPane.showMessageDialog(frame,
                        "wrong password.",
                        "Password error",
                        JOptionPane.ERROR_MESSAGE);
                //System.out.println("wrong password");
                return null;
            }
        }else{
            JOptionPane.showMessageDialog(frame,
                    "no such user.",
                    "Username error",
                    JOptionPane.ERROR_MESSAGE);
        System.out.println("no such user");
    }

        return null;
    }

    public String register(String P_user, String P_pass){
        if (!userPass.containsKey(P_user)){
            userPass.put(P_user,P_pass);
            for (String  name: userPass.keySet()){

                String key =name.toString();
                String value = userPass.get(name).toString();
                System.out.println(key + " " + value);

            }
            return "index.xhtml";
        }else{
            JOptionPane.showMessageDialog(frame,
                    "User already exists.",
                    "Username error",
                    JOptionPane.ERROR_MESSAGE);
            return "User already excits";
        }

    }


}
