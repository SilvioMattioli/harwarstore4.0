package frontDomain;

import java.time.LocalDate;

public class Product {
    private  Integer id;
    private  String manufactory;
    private String name;
    private Float price;
    private LocalDate releasDate;
    private Integer warrenty;
    private Boolean isWireless;

    public Product(Integer id, String manufactory, String name, Float price, LocalDate releasDate, Integer warrenty, Boolean isWireless) {
        this.id = id;
        this.manufactory = manufactory;
        this.name = name;
        this.price = price;
        this.releasDate = releasDate;
        this.warrenty = warrenty;
        this.isWireless = isWireless;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getManufactory() {
        return manufactory;
    }

    public void setManufactory(String manufactory) {
        this.manufactory = manufactory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public LocalDate getReleasDate() {
        return releasDate;
    }

    public void setReleasDate(LocalDate releasDate) {
        this.releasDate = releasDate;
    }

    public Integer getWarrenty() {
        return warrenty;
    }

    public void setWarrenty(Integer warrenty) {
        this.warrenty = warrenty;
    }

    public Boolean getWireless() {
        return isWireless;
    }

    public void setWireless(Boolean wireless) {
        isWireless = wireless;
    }
}
