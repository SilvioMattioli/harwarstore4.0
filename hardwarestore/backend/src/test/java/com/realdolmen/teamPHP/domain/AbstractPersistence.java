package com.realdolmen.teamPHP.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class AbstractPersistence {
    private static EntityManagerFactory emf;

    protected EntityManager em;
    private EntityTransaction etx;

    @BeforeClass
    public static void initializeEntityManagerFactory() {
        // TODO: initialize the EntityManagerFactory
        // The entityManagerFactory will create the tables based on the classes declared in the persistenceUnit
        // in persistence.xml
        emf = Persistence.createEntityManagerFactory("HardwareStorePersistenceUnit");
    }

    @Before
    public void initializeEntityManagerWithTransaction() {
        // TODO: initialize the entity manager from the entity manager factory here
        // The Manager is created by the factory with the correct dialect, url, password and user
        // Plus, the correct tables are being referenced
        System.out.println("Hello before");
        em = emf.createEntityManager();
        // TODO: begin a transaction
        etx = em.getTransaction();
        etx.begin();
    }

    @After
    public void rollbackTransactionAndCloseEntityManager() {
        //When using commit the data that is persisted, can be added to the database
//        etx.commit();
        System.out.println("hello after");
        // TODO: rollback the transaction
        // rollingback means that the data that is persisted, will not be added to the database
        etx.rollback();
        // TODO: close the entity manager
        em.close();
    }

    @AfterClass
    public static void destroyEntityManagerFactory() {
        // TODO; close the EntityManagerFactory

        //etx.commit();
        System.out.println("Hello afterclass");
        emf.close();
    }
}
