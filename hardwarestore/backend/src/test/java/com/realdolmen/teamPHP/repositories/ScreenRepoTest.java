package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.Screen;
import com.realdolmen.teamPHP.domain.ShopBasket;
import com.realdolmen.teamPHP.repositories.ScreenRepo;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class ScreenRepoTest {

    private ScreenRepo screenrepo;
    private static EntityManagerFactory emf;
    private EntityManager em;
    private DateTimeFormatter formatter;

    @BeforeClass
    public static void initCLass()
    {
        emf = Persistence.createEntityManagerFactory("HardwareStorePersistenceUnit");
    }

    @Before
    public void init() {
        System.out.println("init personRepository");
        em = emf.createEntityManager();
        screenrepo = new ScreenRepo(em);
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-DD");
    }


    @Test
    public void findScreenByIdTest() {
        Screen screen = screenrepo.findById(16000L);

        assertEquals((screen).getName(),"Screen Noob");
    }

    @Test
    public  void deleteScreenTest(){
        screenrepo.delete(13000L);
        assertEquals(null,em.find(Screen.class,13000L));
    }
    @Test
    public void updateScreenTest(){
        Screen screen = em.find(Screen.class,16000l);
        screenrepo.begin();
        em.detach(screen);
        screen.setPrice(50.00);
        //hdd.setRpm(500L);
        em.flush();
        em.clear();
        screenrepo.update(screen);
        assertEquals(50.00,(double)em.find(Screen.class,16000L).getPrice(),0);
        //assertNotNull(em.find(HDD.class,1000L));
    }


    @Test
    public void addScreenTest() throws ParseException {
        Screen screen = new Screen();
        screen.setManufactory("manufactory5");
        screen.setName("Harddrive Noob");
        screen.setPrice(7.50);
        LocalDate date1= LocalDate.of(2014, Month.APRIL,1);
        screen.setReleaseDate(date1);
        //System.out.println(screen.getReleaseDate());
        screen.setWarranty(2);
        ShopBasket sb = new ShopBasket();
        sb.setId(5555L);
        screen.setShopBasket(sb);
        screenrepo.save(screen);

        System.out.println("adding "+screen.getClass()+ "To Database");

        Screen ssd1=em.find(Screen.class,screen.getId());
        //assertEquals(5555,ssd1);
        assertEquals("Harddrive Noob",ssd1.getName());
        assertEquals("manufactory5",ssd1.getManufactory());
        assertEquals(date1,ssd1.getReleaseDate());
        assertEquals(2,(int)ssd1.getWarranty());
        //assertEquals(5555,(long)ssd1.getShopBasket().getId());

    }

    @Test
    public void filterScreenOnPriceTest(){
        List<Screen> screens = screenrepo.filterOnPrice(8.00);
        for (Screen screen:screens){
            System.out.println(screen.toString());
        }
        assertTrue(screens.size() >0);
    }

    @After
    public void exit() {
        screenrepo.close();
    }

    @AfterClass
    public static void exitClass(){
        emf.close();
    }
}
