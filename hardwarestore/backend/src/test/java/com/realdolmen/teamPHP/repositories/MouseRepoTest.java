package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.Mouse;
import com.realdolmen.teamPHP.domain.ShopBasket;

import com.realdolmen.teamPHP.repositories.MouseRepo;
import com.realdolmen.teamPHP.repositories.ShopbasketRepo;
import org.junit.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MouseRepoTest {

    private MouseRepo mouseRepo;
    private ShopbasketRepo shopbasketRepo;
    private static EntityManagerFactory emf;
    private EntityManager em;
    private DateTimeFormatter formatter;

    @BeforeClass
    public static void initCLass()
    {
        emf = Persistence.createEntityManagerFactory("HardwareStorePersistenceUnit");
    }

    @Before
    public void init() {
        System.out.println("init personRepository");
        em = emf.createEntityManager();
        mouseRepo = new MouseRepo(em);
        shopbasketRepo = new ShopbasketRepo(em);
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-DD");
    }

    @Test
    public void findMouseByIdTest() {
        Mouse mouse = mouseRepo.findById(9000L);
        assertNotNull(mouse);
        assertEquals(mouse.getName(),"Mouse Master");
    }
    @Test
    public void addMouseTest() throws ParseException {
        ShopBasket sb = new ShopBasket();
        sb.setId(5577L);

        Mouse mouse = new Mouse();
        mouse.setManufactory("manufactory33");
        mouse.setName("Mouse Master P");
        mouse.setPrice(11.50);
        LocalDate date1= LocalDate.of(2014, Month.APRIL,1);
        mouse.setReleaseDate(date1);
        System.out.println(mouse.getReleaseDate());
        mouse.setWarranty(2);


        mouse.setShopBasket(sb);
        mouse.setWireless(true);

        mouseRepo.save(mouse);
//        shopbasketRepo.save(sb);

        System.out.println("adding "+mouse.getClass()+ "To Database");

        Mouse mouse1=em.find(Mouse.class,mouse.getId());
        //assertEquals(5555,keyboard1);
        assertEquals("Mouse Master P",mouse1.getName());
        assertEquals("manufactory33",mouse1.getManufactory());
        assertEquals(date1,mouse1.getReleaseDate());
        assertEquals(2,(int)mouse1.getWarranty());
        //assertEquals(5555,(long)keyboard1.getShopBasket().getId());

    }

    @Test
    public  void deleteKeyboardTest(){
        mouseRepo.delete(12000L);
        assertEquals(null,em.find(Mouse.class,12000L));
    }

    @Test
    public void updateKeyboardTest(){
        Mouse mouse = em.find(Mouse.class,11000l);
        mouseRepo.begin();
        em.detach(mouse);
        mouse.setManufactory("Real Manufactory CTF");
        em.flush();
        em.clear();
        mouseRepo.update(mouse);
        assertEquals("Real Manufactory CTF",em.find(Mouse.class,11000L).getManufactory());
        //assertNotNull(em.find(HDD.class,1000L));
    }

    @Test
    public void filterIswirelessTest(){
        List<Mouse> mouses = mouseRepo.filterOnWirless(false);
       /* for (Keyboard kb:keyboards){
            System.out.println(kb.toString());
        }*/
        mouses.stream().forEach(ms -> System.out.println(ms.toString()));
        assertTrue(mouses.size() >0);
    }

    @Test
    public void filterOnMinMaxPrice(){
        List<Mouse> mouses = mouseRepo.filterPriceBtween(10,20.00);
        /*for (Keyboard kb:mouses){
            System.out.println(kb.toString());
        }*/
        mouses.stream().forEach(ms -> System.out.println(ms.toString()));
        assertTrue(mouses.size() >0);
    }

    @Test
    public void filterManufatoryTest(){
        List<Mouse> muses = mouseRepo.filterManufactuary("Manufactory2");
        /*for (Keyboard kb:keyboards){
            System.out.println(kb.toString());
        }*/
        muses.stream().forEach(ms -> System.out.println(ms.toString()));
     // assertTrue(muses.size() >0);
    }

    @Test
    public void filterPriceAndWirelessTess(){
        List<Mouse> mouses = mouseRepo.filterPriceAndWireless(10.00,20.00,true);
        mouses.stream().forEach(ms -> System.out.println(ms.toString()));
        assertTrue(mouses.size() >0);
    }

   @Test
    public void filterNameLikeTess(){
        List<Mouse> mouses = mouseRepo.filterNameLike("se P");
        mouses.stream().forEach(ms -> System.out.println(ms.toString()));
        assertTrue(mouses.size() >0);
    }

    @After
    public void exit() {
        mouseRepo.close();
    }

    @AfterClass
    public static void exitClass(){
        emf.close();
    }

}
