package com.realdolmen.teamPHP.service;

import com.realdolmen.teamPHP.domain.Keyboard;
import com.realdolmen.teamPHP.domain.Mouse;
import com.realdolmen.teamPHP.repositories.KeyboardRepo;
import com.realdolmen.teamPHP.repositories.MouseRepo;
import com.realdolmen.teamPHP.services.MouseService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;


@RunWith(MockitoJUnitRunner.class)
public class MouseServiceTest {
    @Mock
    private MouseRepo mouserepo;
    private Mouse MouseTest;

    @InjectMocks
    private MouseService mouseService;

    private List<Mouse> mouses;

    @Before
    public void init(){
        mouses = new ArrayList<>();
    }


    @Test
    public void findAllMouseTest() {
        when(mouserepo.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new Mouse())));

        List<Mouse> result = mouseService.findAllMouseService();

        assertEquals(result.size(), 1);

        verify(mouserepo, times(1)).findAll();
        verifyNoMoreInteractions(mouserepo);

    }

}
