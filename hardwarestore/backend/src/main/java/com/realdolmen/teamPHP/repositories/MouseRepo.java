package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.Mouse;

import java.util.List;
import javax.persistence.EntityManager;
import javax.ws.rs.Produces;


@Produces
public class MouseRepo extends AbstractRepoProduct<Mouse,Long> {
    public MouseRepo() {
        super(Mouse.class);
    }

    public MouseRepo(EntityManager em) {
        super(em, Mouse.class);
    }


    @Override
    public Mouse findById(Long id) {
        return super.findById(id);
    }

    public List<Mouse> filterOnWirless(Boolean i) {
        return em.createNamedQuery(Mouse.FILTER_WIRELESS_MOUSE, Mouse.class)
                .setParameter("isWireless", i)
                .getResultList();
    }

    public List<Mouse> filterPriceBtween(double priceMin, double priceMax) {
        return em.createNamedQuery(Mouse.FILTER_PRICE_BETWEEN_MOUSE, Mouse.class)
                .setParameter("priceMin", priceMin).setParameter("priceMax", priceMax)
                .getResultList();
    }

    public List<Mouse> filterManufactuary(String manuFacts) {
        return em.createNamedQuery(Mouse.FILTER_MANUFACTORY_MOUSE, Mouse.class)
                .setParameter("manufactory", manuFacts)
                .getResultList();
    }

    public List<Mouse> filterPriceAndWireless(double priceMin, double priceMax,Boolean wireless) {
        return em.createNamedQuery(Mouse.FILTER_PRICE_AND_WIRELESS_MOUSE, Mouse.class)
                .setParameter("priceMinWireless", priceMin).setParameter("priceMaxWireless", priceMax).setParameter("idwireless",wireless)
                .getResultList();
    }
    public List<Mouse> filterNameLike(String manuFacts) {
        return em.createNamedQuery(Mouse.FILTER_NAME_LIKE, Mouse.class)
                .setParameter("manufactory", manuFacts)
                .getResultList();
    }
}
