package com.realdolmen.teamPHP.repositories;
import com.realdolmen.teamPHP.domain.SSD;

import javax.persistence.EntityManager;
import javax.ws.rs.Produces;
import java.util.List;

@Produces
public class SsdRepo  extends AbstractRepoProduct<SSD,Long>{
    public SsdRepo() {
        super(SSD.class);
    }

    public SsdRepo(EntityManager em) {
        super(em, SSD.class);
    }
    
    
    public List<SSD> filterOnPrice(double i){
        return em.createNamedQuery(SSD.FILTER_PRICE_ABOVE_NUMBER_SSD,SSD.class)
                .setParameter("priceSsd",i)
                .getResultList();
    }

}
