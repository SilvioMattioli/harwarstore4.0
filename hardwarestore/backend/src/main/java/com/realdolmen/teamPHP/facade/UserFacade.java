package com.realdolmen.teamPHP.facade;

import com.realdolmen.teamPHP.domain.UserAccount;
import com.realdolmen.teamPHP.dtos.UserDTO;
import com.realdolmen.teamPHP.mapper.UserDTOMapper;
import com.realdolmen.teamPHP.mapper.UserMapper;
import com.realdolmen.teamPHP.services.UserService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public class UserFacade implements Serializable {
    @Inject
    private UserService userService;

    public UserFacade() {
    }

    @PostConstruct
    public void init(){
        System.out.println("Userfacede constructed");
    }

    public UserDTO findAllUsers(String name){
        UserAccount ua = userService.findUser(name);
        if (ua != null){
            return new UserMapper().apply(ua);
        }
        return  null;
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void saveUser(UserDTO udto){
        userService.addUser(new UserDTOMapper().apply(udto));
    }

}
